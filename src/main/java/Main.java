import com.avenuecode.projecteuller.problems.Problem5;
import com.avenuecode.projecteuller.problems.Problem6;

import java.text.DecimalFormat;

public class Main {

    public static void main(String[] arguments) {

        long answer = Problem6.squareDifference(100);
        System.out.println("ANSWER: " + answer);

    }
}

