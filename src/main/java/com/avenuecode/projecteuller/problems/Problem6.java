package com.avenuecode.projecteuller.problems;
/*The sum of the squares of the first ten natural numbers is,
12+22+...+102=385

The square of the sum of the first ten natural numbers is,
(1+2+...+10)2=552=3025

Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025−385=2640

.

Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.*/
public class Problem6 {
    public static long squareDifference(int lastNaturalNumber)
    {
        long sumOfEachSquare = 0;
        long sumOfAllNumbersSquared = 0;
        for(int currentNaturalNumber = 1; currentNaturalNumber <= lastNaturalNumber;currentNaturalNumber++)
        {
            sumOfEachSquare += Math.pow(currentNaturalNumber,2);
            sumOfAllNumbersSquared += currentNaturalNumber;
        }
        sumOfAllNumbersSquared = (long)Math.pow(sumOfAllNumbersSquared, 2);
        return sumOfAllNumbersSquared - sumOfEachSquare;
    }
}
