package com.avenuecode.projecteuller.problems;
/*

2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
*/

public class Problem5 {
    public static int smallestProduct(int lowerBound, int upperBound) {

        if (lowerBound > upperBound) {
            int swap = upperBound;
            upperBound = lowerBound;
            lowerBound = swap;
        }

        int smallestProduct = upperBound;
        int multiplier = 1;
        int factor = 0;

        do {

            int product = upperBound * multiplier;

            for (factor = upperBound; factor >= lowerBound; factor--) {
                if (product % factor != 0)
                    break;
            }

            if (factor < lowerBound)
                smallestProduct = product;
            else
                multiplier++;

        } while (factor >= lowerBound);

        return smallestProduct;


    }
}
