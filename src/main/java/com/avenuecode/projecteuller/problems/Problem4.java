package com.avenuecode.projecteuller.problems;
/*


A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.

 */
public class Problem4 {
    public static int getLargestPalindrome(int factorsQuantity)
    {

        int upperBound = ((int)Math.pow(10,factorsQuantity)) - 1;
        int lowerBound =  ((int)Math.pow(10,factorsQuantity - 1)) - 1;

        int max_prdt = 0;

        for (int i = upperBound; i >= lowerBound; i--)
        {
            for (int j = i; j >= lowerBound; j--)
            {
                // calculating product of two numbers
                int product = i * j;
                if (product < max_prdt)
                    break;
                int number = product;
                int reverse_num = 0;
                // checking palindrome
                while (number != 0)
                {
                    reverse_num = reverse_num * 10 + number % 10;
                    number /= 10;
                }
                // updating the max_prdt
                if (product == reverse_num && product > max_prdt)
                    max_prdt = product;
            }
        }
        return max_prdt;
    }
}
