package com.avenuecode.projecteuller.problems;
/*
Largest prime factor
Problem 3

The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
*/
//Math Solution found in https://www.mathsisfun.com/prime-factorization.html

import javax.swing.text.MutableAttributeSet;

public class Problem3 {
    public static long getLargestPrimeFactor(int limit)
    {
       long lastFactor;

       if(limit % 2 == 0)
       {
           lastFactor = 2l;
           limit = limit / 2;

           while(limit % 2 == 0)
                limit = limit / 2;
       }
       else
           lastFactor = 1l;


      for(int factor = 3; factor <= Math.sqrt(limit); factor += 2)
      {
          if(limit % factor == 0)
          {
              limit = limit / factor;
              lastFactor = factor;

              while(limit % factor == 0)
                  limit = limit / factor;
          }
      }

      if(limit == 1)
          return lastFactor;
      else
          return limit;

    }

}
