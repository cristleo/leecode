package com.avenuecode.amcat.challenges;

import java.util.Arrays;

public class GreatestCommonDivisor {
    public int generalizedGCD(int num, int[] arr)
    {
        int maxValue = Arrays.stream(arr).min().getAsInt();

        for(int j = maxValue; j > 0; j--)
           for(int i = arr[1]; i < arr.length; i++)
           {
               if(arr[i] % maxValue != 0)
               {
                   maxValue--;
                   break;
               }
           }

        return maxValue;
    }
}
