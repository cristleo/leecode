package com.avenuecode.amcat.challenges;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class EightHouses {
    public List<Integer> cellCompete(final int[] states, int days) {
        int[] values = states;
        Integer[] memory = Arrays.stream(states).boxed().toArray(Integer[]::new);

        while (days > 0) {
            memory[0] = values[1] ^ 0;
            memory[7] = values[states.length - 2] ^ 0;

            for (int i = 1; i < values.length - 1; i++) {
                memory[i] = values[i - 1] ^ values[i + 1];
            }
            values = Arrays.stream(memory).mapToInt(Integer::intValue).toArray();
            days--;
        }


        return Arrays.stream(values).boxed().collect(Collectors.toList());

        // WRITE YOUR CODE HERE
    }
}
