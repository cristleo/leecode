package com.avenuecode.leetcode.challenges;
/*
Given a list of unique words, find all pairs of distinct indices (i, j) in the given list,
so that the concatenation of the two words, i.e. words[i] + words[j] is a palindrome.

Example 1:

Input: ["abcd","dcba","lls","s","sssll"]
Output: [[0,1],[1,0],[3,2],[2,4]]
Explanation: The palindromes are ["dcbaabcd","abcddcba","slls","llssssll"]
Example 2:

Input: ["bat","tab","cat"]
Output: [[0,1],[1,0]]
Explanation: The palindromes are ["battab","tabbat"]
*/

import java.util.*;

public class Challenge366 {

    public List<List<Integer>> palindromePairs(String[] words) {

        List<List<Integer>> pairs = new ArrayList<List<Integer>>();
        ArrayList<Integer> transientPairs;

        for(int i = 0; i < words.length; i++)
        {
            for(int j = i + 1; j < words.length; j++)
            {
                if(isPalindrome(words[i] + words[j]))
                {
                    transientPairs =  new ArrayList<Integer>();
                    transientPairs.add(i);
                    transientPairs.add(j);
                    pairs.add(transientPairs);
                }

                if(isPalindrome(words[j] + words[i]))
                {
                    transientPairs =  new ArrayList<Integer>();
                    transientPairs.add(j);
                    transientPairs.add(i);
                    pairs.add(transientPairs);
                }
            }
        }

        return pairs;

    }

    public boolean isPalindrome(String word)
    {
        boolean answer = true;
        int rigthPosition = word.length() - 1;
        int leftPosition = 0;

        while(rigthPosition > leftPosition)
        {
            if(word.charAt(rigthPosition) != word.charAt(leftPosition))
                return false;
            rigthPosition--;
            leftPosition++;
        }

        return answer;
    }
}
