package com.avenuecode.leetcode.challenges;
/*
Given an array arr, replace every element in that array with the greatest element among the elements to its right,
and replace the last element with -1.

After doing so, return the array.

Example 1:

Input: arr = [17,18,5,4,6,1]
Output: [18,6,6,6,1,-1]
*/
public class Challenge1299 {
    public int[] replaceElements(int[] arr) {
        int greatestElement = arr[arr.length - 1];
        arr[arr.length - 1] = -1;

        for(int pointer = arr.length - 2; pointer >= 0;pointer--)
        {
            int maxSwap = Math.max(greatestElement, arr[pointer]);
            arr[pointer] = greatestElement;
            greatestElement = maxSwap;
        }

        return arr;
    }
}
