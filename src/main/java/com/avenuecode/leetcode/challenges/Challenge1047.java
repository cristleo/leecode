package com.avenuecode.leetcode.challenges;

public class Challenge1047 {
    public String removeDuplicates(String S) {
        StringBuilder sb = new StringBuilder(S);
        int position = 0;

        while (position + 1 < sb.length()) {

            if (sb.charAt(position) == sb.charAt(position + 1)) {
                sb.deleteCharAt(position);
                sb.deleteCharAt(position);

                if(position - 1 >= 0)
                    position = position - 1;
            } else {
                position++;
            }
        }
        return sb.toString();
    }
}
