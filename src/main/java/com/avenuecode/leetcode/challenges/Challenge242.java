package com.avenuecode.leetcode.challenges;
/*
Given two strings s and t , write a function to determine if t is an anagram of s.

Example 1:

Input: s = "anagram", t = "nagaram"
Output: true
Example 2:

Input: s = "rat", t = "car"
Output: false
Note:
You may assume the string contains only lowercase alphabets.

Follow up:
What if the inputs contain unicode characters? How would you adapt your solution to such case?
*/
import java.util.Arrays;

public class Challenge242 {
    public boolean isAnagram(String s, String t) {

        if (s.length() != t.length())
            return false;

        char[] firstSequence = s.toCharArray();
        char[] secondSequence = t.toCharArray();
        Arrays.sort(firstSequence);
        Arrays.sort(secondSequence);

        for (int position = 0; position < firstSequence.length; position++) {
            if (firstSequence[position] != secondSequence[position])
                return false;
        }

        return true;
    }
}
