package com.avenuecode.leetcode.challenges;

/*
Design an Iterator class, which has:

A constructor that takes a string characters of sorted distinct lowercase English letters and a number
combinationLength as arguments.
A function next() that returns the next combination of length combinationLength in lexicographical order.
A function hasNext() that returns True if and only if there exists a next combination.

Example:

CombinationIterator iterator = new CombinationIterator("abc", 2); // creates the iterator.

iterator.next(); // returns "ab"
iterator.hasNext(); // returns true
iterator.next(); // returns "ac"
iterator.hasNext(); // returns true
iterator.next(); // returns "bc"
iterator.hasNext(); // returns false

Constraints:

1 <= combinationLength <= characters.length <= 15
There will be at most 10^4 function calls per test.
It's guaranteed that all calls of the function next are valid.
*/
public class Challenge1286 {

    int combinationLength;
    String characters;
    int[] selectedCharsPositions;
    boolean hasNext;

    public Challenge1286(String characters, int combinationLength) {
        this.characters = characters;
        this.combinationLength = combinationLength;
        this.hasNext = calculateNext();
    }

    private boolean calculateNext() {
        if (selectedCharsPositions == null) {
            selectedCharsPositions = new int[combinationLength];
            for (int i = 0; i < combinationLength; i++)
                selectedCharsPositions[i] = i;

            return true;
        } else {
            for (int i = combinationLength - 1; i >= 0; i--) {
                int limit = (characters.length() - combinationLength) + i;

                if (selectedCharsPositions[i] < limit) {
                    selectedCharsPositions[i]++;

                    for (int j = i + 1; j < combinationLength; j++) {
                        selectedCharsPositions[j] = selectedCharsPositions[i] + (j - i);
                    }
                    return true;
                }
            }
            return false;
        }
    }

    public String next()
    {
        char[] combination = new char[combinationLength];
        for(int i = 0; i <  combinationLength; i++)
        {
            combination[i] = characters.charAt(selectedCharsPositions[i]);
        }
        this.hasNext = calculateNext();
        return new String(combination);

    }

    public boolean hasNext()
    {
        return hasNext;
    }
}

