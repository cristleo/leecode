package com.avenuecode.leetcode.challenges;


import javax.naming.PartialResultException;
import java.text.CollationElementIterator;
import java.util.Collections;

/*
Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.
(i.e.,  [0,1,2,4,5,6,7] might become  [4,5,6,7,0,1,2]).

Find the minimum element.

You may assume no duplicate exists in the array.

Example 1:

Input: [3,4,5,1,2]
Output: 1
Example 2:

Input: [4,5,6,7,0,1,2]
Output: 0
 */
public class Challenge153 {
    public int findMin(int[] nums) {
        int minimumValue = Integer.MAX_VALUE;

        for(int index = 0; index < nums.length; index++)
        {
            if(nums[index] < minimumValue)
                minimumValue = nums[index];
        }

        return minimumValue;


    }

    public int findMinDivideApproach(int[] nums)
    {
        int limitSteps = nums.length / 2;
        int minimumLeftSide = nums[limitSteps];
        int minimumRightSide = nums[limitSteps];

        for(int index = 0; index <= limitSteps; index++)
        {
            if(limitSteps - index >=0 && minimumLeftSide > nums[limitSteps - index])
                minimumLeftSide = nums[limitSteps - index];

            if(limitSteps + index <= nums.length - 1 && minimumRightSide > nums[limitSteps + index])
                minimumRightSide = nums[limitSteps + index];
        }

        if(minimumLeftSide <= minimumRightSide)
            return minimumLeftSide;
        else
            return minimumRightSide;
    }
}
