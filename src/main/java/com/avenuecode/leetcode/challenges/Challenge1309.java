package com.avenuecode.leetcode.challenges;

import java.util.Queue;
import java.util.Stack;

public class Challenge1309 {
    public String freqAlphabets(String s) {

        StringBuilder decodedPhrase =  new StringBuilder();
        int position = 0;

      while(position < s.length())
        {
            if(s.length() - position >= 3 && s.charAt(position + 2) == '#')
            {
                String chars = s.substring(position, position + 2);
                int letterCode = Integer.valueOf(chars) + 96;
                decodedPhrase.append((char)(letterCode));
                position+=3;
            }
            else
            {
                int letterCode = Character.getNumericValue(s.charAt(position)) + 96;
                decodedPhrase.append((char)(letterCode));
                position++;
            }
        }

        return decodedPhrase.toString();
    }
}
