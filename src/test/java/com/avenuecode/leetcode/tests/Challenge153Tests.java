package com.avenuecode.leetcode.tests;

import com.avenuecode.leetcode.challenges.Challenge153;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class Challenge153Tests {

    Challenge153 challenge;

    @BeforeEach
    private void setup()
    {
        challenge = new Challenge153();
    }

    @Test
    public void givenFirstExample()
    {
        int[] given = new int[]{3,4,5,1,2};
        int expected = 1;
        Assertions.assertEquals(expected, challenge.findMin(given));
    }

    @Test
    public void givenSecondExample()
    {
        int[] given = new int[]{4,5,6,7,0,1,2};
        int expected = 0;
        Assertions.assertEquals(expected, challenge.findMin(given));
    }

    @Test
    public void givenFirstExampleExpectedWithDivideApproach()
    {
        int[] given = new int[]{3,4,5,1,2};
        int expected = 1;
        Assertions.assertEquals(expected, challenge.findMinDivideApproach(given));
    }

    @Test
    public void givenSecondExampleWithDivideApproach()
    {
        int[] given = new int[]{4,5,6,7,0,1,2};
        int expected = 0;
        Assertions.assertEquals(expected, challenge.findMinDivideApproach(given));
    }
}
