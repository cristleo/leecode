package com.avenuecode.leetcode.tests;

import com.avenuecode.leetcode.challenges.Challenge1370;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class Challenge1370Tests {

    Challenge1370 challenge;

    @BeforeAll
    public void setup()
    {
        challenge =  new Challenge1370();
    }

    @ParameterizedTest
    @CsvSource(value = {
            "aaaabbbbcccc:abccbaabccba",
            "rat:art",
            "leetcode:cdelotee",
            "ggggggg:ggggggg",
            "spo:ops",
    },
            delimiter = ':')
    public void allExamplesGiven(String input, String expected)
    {
        Assertions.assertEquals(expected, challenge.sortString(input));
    }

    @Test
    public void firstHiddenTest()
    {
        String input = "rdoqnydsalrccrlasdynqodr";
        String expected = "acdlnoqrsyysrqonldcadrrd";
        Assertions.assertEquals(expected, challenge.sortString(input));
    }


}
