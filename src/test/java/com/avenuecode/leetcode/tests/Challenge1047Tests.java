package com.avenuecode.leetcode.tests;

import com.avenuecode.leetcode.challenges.Challenge1047;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class Challenge1047Tests {
    Challenge1047 challenge;

    @BeforeEach
    public void setup()
    {
        challenge = new Challenge1047();
    }

    @ParameterizedTest
    @CsvSource(value = {
            "abbaca:ca",
            "azxxzy:ay",
            "ibfjcaffccadidiaidchakchchcahabhibdcejkdkfbaeeaecdjhajbkfebebfea:ibfjcdidiaidchakchchcahabhibdcejkdkfbecdjhajbkfebebfea"

    },
            delimiter = ':')
    public void givenExamples(String input, String expected)
    {
        Assertions.assertEquals(expected, challenge.removeDuplicates(input));
    }

    @Test
    public void givenExamples()
    {
        String expected = "";
        Assertions.assertEquals(expected, challenge.removeDuplicates("aaaaaaaa"));
    }


}

