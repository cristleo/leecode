package com.avenuecode.leetcode.tests;

import com.avenuecode.leetcode.challenges.Challenge1286;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class Challenge1286Tests {
    private Challenge1286 challenge;

    @Test
    public void givenFirstExample() {
        challenge = new Challenge1286("abc", 2);
        String expectedFirstNextCall = "ab";
        String expectedSecondNextCall = "ac";
        String expectedThirdNextCall = "bc";

        Assertions.assertEquals(expectedFirstNextCall, challenge.next());
        Assertions.assertTrue(challenge.hasNext());
        Assertions.assertEquals(expectedSecondNextCall, challenge.next());
        Assertions.assertTrue(challenge.hasNext());
        Assertions.assertEquals(expectedThirdNextCall, challenge.next());
        Assertions.assertFalse(challenge.hasNext());


    }

    @Test
    public void firstTestOnSubmit() {
        challenge = new Challenge1286("chp", 1);
        String expectedFirstNextCall = "c";
        String expectedSecondNextCall = "h";
        String expectedThirdNextCall = "p";

        Assertions.assertTrue(challenge.hasNext());
        Assertions.assertEquals(expectedFirstNextCall, challenge.next());

        Assertions.assertTrue(challenge.hasNext());
        Assertions.assertTrue(challenge.hasNext());

        Assertions.assertEquals(expectedSecondNextCall, challenge.next());
        Assertions.assertEquals(expectedThirdNextCall, challenge.next());

        Assertions.assertFalse(challenge.hasNext());
        Assertions.assertFalse(challenge.hasNext());
        Assertions.assertFalse(challenge.hasNext());
        Assertions.assertFalse(challenge.hasNext());

    }
    @Test
    public void secondTestOnSubmit()
    {
        challenge = new Challenge1286("gkosu", 3);
        String expectedFirstNextCall = "gko";
        String expectedSecondNextCall = "gks";
        String expectedThirdNextCall = "gku";
        String expectedFourthNextCall = "gos";
        String expectedFifthNextCall = "gou";

        Assertions.assertTrue(challenge.hasNext());
        Assertions.assertTrue(challenge.hasNext());
        Assertions.assertTrue(challenge.hasNext());

        Assertions.assertEquals(expectedFirstNextCall, challenge.next());

        Assertions.assertTrue(challenge.hasNext());

        Assertions.assertEquals(expectedSecondNextCall, challenge.next());
        Assertions.assertEquals(expectedThirdNextCall, challenge.next());
        Assertions.assertEquals(expectedFourthNextCall, challenge.next());

        Assertions.assertTrue(challenge.hasNext());

        Assertions.assertEquals(expectedFifthNextCall, challenge.next());


    }

}
