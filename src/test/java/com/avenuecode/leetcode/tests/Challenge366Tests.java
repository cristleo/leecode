package com.avenuecode.leetcode.tests;

import com.avenuecode.leetcode.challenges.Challenge366;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.List;

public class Challenge366Tests {

    Challenge366 challenge;

    @BeforeEach
    public void setup()
    {
        challenge = new Challenge366();
    }

    @Test
    public void givenFirstExample()
    {
        String[] given = new String[]{"abcd","dcba","lls","s","sssll"};
        List<List<Integer>> expected = new ArrayList<List<Integer>>();

        expected.add(new ArrayList<Integer>() {
            {
                add(0);
                add(1);
            }
        });
        expected.add(new ArrayList<Integer>() {
            {
                add(1);
                add(0);
            }
        });
        expected.add(new ArrayList<Integer>() {
            {
                add(3);
                add(2);
            }
        });

        expected.add(new ArrayList<Integer>() {
            {
                add(2);
                add(4);
            }
        });

        List<List<Integer>> result = challenge.palindromePairs(given);

        Assertions.assertEquals(expected, result);

    }

    @Test
    public void giverSecondExample()
    {
        String[] given = new String[]{"bat","tab","cat"};
        List<List<Integer>> expected = new ArrayList<List<Integer>>();
        expected.add(new ArrayList<Integer>() {
            {
                add(0);
                add(1);
            }
        });
        expected.add(new ArrayList<Integer>() {
            {
                add(1);
                add(0);
            }
        });

        List<List<Integer>> result = challenge.palindromePairs(given);

        Assertions.assertEquals(expected, result);

    }

    @ParameterizedTest
    @ValueSource(strings = {"ABBA","OVO","abcddcba", "battab", "S","AA"})
    public void givenPalindromeShouldReturnTrue(String palindrome)
    {
        Assertions.assertTrue(challenge.isPalindrome(palindrome));
    }

    @ParameterizedTest
    @ValueSource(strings = {"CASCA","abacaxi","llss", "batata"})
    public void givenNoPalindromeShouldReturnFalse(String noPalindrome)
    {
        Assertions.assertFalse(challenge.isPalindrome(noPalindrome));
    }


}
