package com.avenuecode.leetcode.tests;

import com.avenuecode.leetcode.challenges.Challenge242;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class Challenge242Tests {

    Challenge242 challenge;
    @BeforeEach
    public void setup()
    {
        challenge = new Challenge242();
    }

    @ParameterizedTest
    @CsvSource(value = {
            "anagram:nagaram",
            "rato:tora"
            },
            delimiter = ':')
    public void examplesShouldReturnTrue(String firstSequence, String secondSequence)
    {
        Assertions.assertTrue(challenge.isAnagram(firstSequence, secondSequence));
    }

    @ParameterizedTest
    @CsvSource(value = {"rat:car","rato:tara"},
    delimiter = ':')
    public void examplesShouldReturnFalse(String firstSequence, String secondSequence)
    {
        Assertions.assertFalse(challenge.isAnagram(firstSequence,secondSequence));
    }
}
