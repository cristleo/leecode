package com.avenuecode.leetcode.tests;

import com.avenuecode.leetcode.challenges.Challenge1299;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class Challenge1299Tests {

    Challenge1299 challenge;

    @BeforeEach
    private void setup()
    {
        challenge = new Challenge1299();
    }

    @Test
    public void givenExampleInputOutput()
    {
        int[] given = new int[]{17,18,5,4,6,1};
        int[] expected = new int[]{18,6,6,6,1,-1};

        Assertions.assertArrayEquals(expected, challenge.replaceElements(given));
    }

}
