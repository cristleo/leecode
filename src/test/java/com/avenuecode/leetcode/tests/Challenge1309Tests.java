package com.avenuecode.leetcode.tests;

import com.avenuecode.leetcode.challenges.Challenge1309;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class Challenge1309Tests {
    Challenge1309 challenge;

    @BeforeEach
    public void setup()
    {
        challenge =  new Challenge1309();
    }

    @ParameterizedTest
    @CsvSource(value = {
            "jkab:10#11#12",
            "acz:1326#",
            "y:25#",
            "abcdefghijklmnopqrstuvwxyz:12345678910#11#12#13#14#15#16#17#18#19#20#21#22#23#24#25#26#"
    }, delimiter = ':')
    public void givenExamples(String expected, String input)
    {
        Assertions.assertEquals(expected, challenge.freqAlphabets(input));
    }
}
