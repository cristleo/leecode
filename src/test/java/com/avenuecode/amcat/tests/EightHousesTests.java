package com.avenuecode.amcat.tests;

import com.avenuecode.amcat.challenges.EightHouses;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EightHousesTests {

    EightHouses challenge;

    @BeforeEach
    public void setup()
    {
        challenge = new EightHouses();
    }

    @Test
    public void firstExample()
    {
        int[] input = new int[]{1,0,0,0,0,1,0,0};
        ArrayList<Integer> expected = new ArrayList<>(Arrays.asList(0,1,0,0,1,0,1,0));
        Assertions.assertEquals(expected, challenge.cellCompete(input,1));

    }
    @Test
    public void secondExample()
    {
        int[] input = new int[]{1,1,1,0,1,1,1,1};
        ArrayList<Integer> expected = new ArrayList<>(Arrays.asList(0,0,0,0,0,1,1,0));
        Assertions.assertEquals(expected, challenge.cellCompete(input,2));

    }
}
