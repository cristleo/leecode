package com.avenuecode.amcat.tests;

import com.avenuecode.amcat.challenges.GreatestCommonDivisor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class GreatestCommonDivisorTests {
    GreatestCommonDivisor challenge;

    @BeforeEach
    public void setup()
    {
        challenge = new GreatestCommonDivisor();
    }

    @Test
    public void firstExample()
    {
        int[] input = new int[]{2,3,4,5,6};
        int expected = 1;
        Assertions.assertEquals(expected, challenge.generalizedGCD(5,input));
    }
    @Test
    public void secondExample()
    {
        int[] input = new int[]{2,4,6,8,10};
        int expected = 2;
        Assertions.assertEquals(expected, challenge.generalizedGCD(5,input));
    }
}
