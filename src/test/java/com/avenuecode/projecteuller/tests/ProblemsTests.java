package com.avenuecode.projecteuller.tests;

import com.avenuecode.projecteuller.problems.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ProblemsTests {

    @Test
    public void problem1Tests()
    {
        int expected = 23;
        Assertions.assertEquals(expected, Problem1.sumOfTerms(10) );
    }

    @Test
    public void problem2Tests()
    {
        int expected = 10;
        Assertions.assertEquals(expected, Problem2.sumEvenFibonnacciElementsUntil(10));
    }

    @Test
    public void problem3Tests()
    {
        int expected = 29;
        Assertions.assertEquals(expected, Problem3.getLargestPrimeFactor(13195));
        Assertions.assertEquals(7, Problem3.getLargestPrimeFactor(147));
        Assertions.assertEquals(3, Problem3.getLargestPrimeFactor(12));
    }

    @Test
    public void problem4Tests()
    {
        int expected = 9009;
        Assertions.assertEquals(expected, Problem4.getLargestPalindrome(2));
    }


    @Test
    public void problem5Tests()
    {
        int expected = 2520;
        Assertions.assertEquals(expected, Problem5.smallestProduct(1,10));
    }

    @Test
    public void problem5TestsLower1Upper4()
    {
        int expected = 12;
        Assertions.assertEquals(expected, Problem5.smallestProduct(1,4));
    }


    @Test
    public void problem5TestsLower3Upper4()
    {
        int expected = 12;
        Assertions.assertEquals(expected, Problem5.smallestProduct(3,4));
    }
    @Test
    public void problem5TestsLower1Upper20()
    {
        int expected = 232792560;
        Assertions.assertEquals(expected, Problem5.smallestProduct(1,20));
    }

    @Test
    public void problem6TestsFirst10Difference()
    {
        double expected = 2640;
        Assertions.assertEquals(expected, Problem6.squareDifference(10));
    }
}
